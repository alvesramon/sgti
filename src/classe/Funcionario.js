class Funcionario{
    constructor(nome, matricula){
        this.nome = nome
        this.matricula = matricula
    }
}

class Motorista extends Funcionario, Usuario{
    constructor(nome, matricula, cnh){
        super(nome, matricula)
        this.cnh = cnh
    }
}

class Administrativo extends Funcionario, Usuario{
    constructor(nome, matricula, funcao){
        super(nome, matricula)
        this.funcao = funcao
    }
}

class Solicitante extends Funcionario, Usuario{
    constructor(nome, matricula, funcao, setor){
        super(nome, matricula)
        this.funcao = funcao
        this.setor = setor
    }
}

class Passageiro extends Funcionario{
    constructor(nome, matricula, funcao){
        super(nome, matricula)
        this.funcao = funcao
    }
}