class Local{
    constructor(nome, endereco){
        this.nome = nome
        this.endereco = endereco
    }
}

class LocalDeOrigem extends Local{
    constructor(nome, endereco, horarioDeSaida, horarioDeRetorno){
        super(nome, endereco)
        this.horarioDeSaida = horarioDeSaida
        this.horarioDeRetorno = horarioDeRetorno
    }
}

class LocalDeDestino extends Local{
    constructor(nome, endereco, horarioDeChegada, horarioDeSaida){
        super(nome, endereco)
        this.horarioDeChegada = horarioDeChegada
        this.horarioDeSaida = horarioDeSaida
    }
}