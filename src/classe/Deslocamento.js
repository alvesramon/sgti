class Deslocamento{
    constructor(data, horarioDePartida, locaDeOrigem, locaDeDestino, veiculo, passageiro, motorista){
        this.data = data
        this.horarioDePartida = horarioDePartida
        this.locaDeOrigem = locaDeOrigem
        this.locaDeDestino = locaDeDestino
        this.veiculo = veiculo
        this.passageiro = passageiro
        this.motorista = motorista
    }
}