const express = require('express');
const routes = express.Router();

let db = [];

routes.get('/login', (req, res) => {

});

routes.post('/register', (req, res) => {
    const body = req.body;

    if (!body)
        return res.status(400).end();

    db.push(body);
    return res.json(body);
});

module.exports = routes