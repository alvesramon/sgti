const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./src/config/routes');
const mongoose = require('mongoose');

const app = express();

mongoose.connect('mongodb+srv://admin_ramon:sgti@cluster0.l1rla.mongodb.net/test')

app.use(cors());
app.use(routes);
app.use(express.json());

app.listen(8080, () => {
    console.log("[SERVER ONLINE]");
});
